/**
 * Сущность нашего поста.
 * Сущность -- это элемент предметной области и через этот тип мы его описываем.
 */
export type UserEntity = {
   id: string,
   email: string,
   name: string,
   age?: number | null,
   phone?: string | null,
   updatedAt: string;
   createdAt: string;
};
