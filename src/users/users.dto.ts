import { UserEntity } from './user.entity';

export type CreateUserDto = Pick<UserEntity, 'age' | 'email' | 'name' | 'phone'>;
export type UpdateUserDto = Partial<CreateUserDto>;

